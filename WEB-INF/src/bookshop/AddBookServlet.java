package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;

@WebServlet("/manage/addbook")
public class AddBookServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }

    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        Object _book = request.getAttribute("book");
        if (null == _book) {
            Book book = new Book();
            request.setAttribute("book", book);
        }
        request.getRequestDispatcher("/WEB-INF/jsp/manage/add_book.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        Book book = new Book();
        String bookid = Long.toString(2940830 + System.currentTimeMillis()).substring(0,10);
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String publisher = request.getParameter("publisher");
        String publishdate = request.getParameter("publishdate");
        String isbn = request.getParameter("isbn");
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        String authorinfo = request.getParameter("authorinfo");
        String content = request.getParameter("content");
        String cover = request.getParameter("cover");
        String price = request.getParameter("price");

        book.setBookid(bookid);
        book.setTitle(title);
        book.setAuthor(author);
        book.setPublisher(publisher);
        book.setPublishdate(publishdate);
        book.setIsbn(isbn);
        book.setCategory(category);
        book.setDescription(description);
        book.setAuthorinfo(authorinfo);
        book.setContent(content);
        book.setCover(cover);
        book.setImages("");
        book.setPrice(Float.parseFloat(price));

        boolean found = false;
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList = (ArrayList<Book>)request.getSession().getAttribute("bookList");
        for (Book tmp: bookList)
            if (bookid.equals(tmp.getBookid().trim()))
                found = true;
        if (found) {
            request.getSession().setAttribute("errormsg", "book exists");
            request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        } else {
            try {
                dbconn = dataSource.getConnection();
                String sql = "INSERT INTO books(bookid, title, author, publisher, publishdate, isbn, category, description, authorinfo, content, cover, price) VALUES ('";
                sql += bookid + "', '"
                    + title + "', '"
                    + author + "', '"
                    + publisher + "', '"
                    + publishdate + "', '"
                    + isbn + "', '"
                    + category + "', '"
                    + description + "', '"
                    + authorinfo + "', '"
                    + content + "', '"
                    + cover + "', "
                    + price + ")";

                pstmt = dbconn.prepareStatement(sql);
                int changedLine = pstmt.executeUpdate();
                if (changedLine > 0) {
                    bookList.add(book);
                    request.getSession().setAttribute("bookList", bookList);
                    request.setAttribute("book", book);
                    response.sendRedirect("/bookshop/product/details?id=" + bookid);
                } else {
                    request.getSession().setAttribute("errormsg", "add book failed");
                    request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try { if (null!=pstmt) pstmt.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=dbconn) dbconn.close();}
                catch (SQLException e) {e.printStackTrace();}
            }
        }
    }
}

