package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;

@WebServlet("/index.jsp")
public class IndexServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        User user = new User();
        Object _user = request.getSession().getAttribute("user");
        if ( null != _user )
            user = (User) _user;

        int num = 20;
        ResultSet rst = null;
        try {
            dbconn = dataSource.getConnection();
            String sql = "SELECT * FROM books ORDER BY publishdate DESC LIMIT ?";
            pstmt = dbconn.prepareStatement(sql);
            pstmt.setInt(1, num);
            rst = pstmt.executeQuery();
            ArrayList<Book> bookList = new ArrayList<Book>();
            bookList = setBookInfo(rst);
            if (!bookList.isEmpty()) {
                request.getSession().setAttribute("bookList", bookList);
                request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { if (null!=rst) rst.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=pstmt) pstmt.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn) dbconn.close();}
            catch (SQLException e) {e.printStackTrace();}
        }
    }

    private ArrayList<Book> setBookInfo(ResultSet rst) {
        ArrayList<Book> bookList = new ArrayList<Book>();
        try {
            while (rst.next()) {
                Book book = new Book();
                book.setBookid(rst.getString("bookid"));
                book.setTitle(rst.getString("title"));
                book.setAuthor(rst.getString("author"));
                book.setPublisher(rst.getString("publisher"));
                book.setPublishdate(rst.getString("publishdate"));
                book.setIsbn(rst.getString("isbn"));
                book.setCategory(rst.getString("category"));
                book.setDescription(rst.getString("description").replaceAll("(\r\n|\n)", "<br />"));
                book.setAuthorinfo(rst.getString("authorinfo").replaceAll("(\r\n|\n)", "<br />"));
                book.setContent(rst.getString("content").replaceAll("(\r\n|\n)", "<br />"));
                book.setCover(rst.getString("cover"));
                book.setImages(rst.getString("images"));
                book.setPrice(rst.getFloat("price"));
                book.setRating(rst.getFloat("rating"));
                book.setInstock(rst.getInt("instock"));
                bookList.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookList;
    }
}

