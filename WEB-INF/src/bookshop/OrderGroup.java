package bookshop;

import java.io.Serializable;
import java.util.ArrayList;
import bookshop.Order;

public class OrderGroup implements Serializable
{
    private String groupid;
    private ArrayList<Order> orders;
    private float totalprice;

    public OrderGroup() {
        this.groupid = "";
        this.totalprice = 0;
        this.orders = new ArrayList<Order>();
    }
    public OrderGroup(Order order) {
        this.groupid = order.getMainid().trim();
        this.orders = new ArrayList<Order>();
        this.orders.add(order);
        this.totalprice += order.getPerprice() * order.getQty();
    }

    public int size() {
        return this.orders.size();
    }

    public boolean add(Order order) {
        String mainid = order.getMainid().trim();
        if (this.groupid == "")
            this.groupid = mainid;
        if (this.groupid.equals(mainid)) {
            this.orders.add(order);
            this.totalprice += order.getPerprice() * order.getQty();
            return true;
        } else
            return false;
    }

    public Order getMainOrder() {
        for (Order tmp: this.orders)
            if (tmp.getOrderid().trim().equals(this.groupid))
                return tmp;
        return null;
    }

    public String getGroupid() {
        return this.groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public ArrayList<Order> getOrders() {
        return this.orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public float getTotalprice() {
        return this.totalprice;
    }

    public void setTotalprice(float totalprice) {
        this.totalprice = totalprice;
    }
}
