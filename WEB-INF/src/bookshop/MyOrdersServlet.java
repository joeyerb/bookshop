package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;
import bookshop.Order;
import bookshop.OrderGroup;

@WebServlet("/myorders")
public class MyOrdersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        User user = new User();
        Object _user = request.getSession().getAttribute("user");
        if (_user != null) {
            user = (User) _user;
            ResultSet rst = null;
            try {
                dbconn = dataSource.getConnection();
                String sql = "SELECT * FROM orders WHERE userid = '" + user.getUserid() + "' ORDER BY mainid DESC";
                pstmt = dbconn.prepareStatement(sql);
                rst = pstmt.executeQuery();
                HashMap<String, OrderGroup> myOrders = new HashMap<String, OrderGroup>();
                while (rst.next()) {
                    Order order = new Order();
                    order.setOrderid(rst.getString("orderid"));
                    order.setBookid(rst.getString("bookid"));
                    order.setOrderdate(rst.getString("orderdate"));

                    order.setTitle(rst.getString("title"));
                    order.setCover(rst.getString("cover"));
                    order.setPerprice(rst.getFloat("perprice"));
                    order.setQty(rst.getInt("qty"));

                    String mainid = rst.getString("mainid");
                    order.setMainid(mainid);

                    order.setUserid(rst.getString("userid"));
                    order.setEmail(rst.getString("email"));
                    order.setAddress(rst.getString("address"));
                    order.setPhone(rst.getString("phone"));

                    order.setStatus(rst.getBoolean("status"));

                    OrderGroup orderGroup = myOrders.get(mainid);
                    if (orderGroup != null) {
                        orderGroup.add(order);
                    } else {
                        orderGroup = new OrderGroup(order);
                        myOrders.put(mainid, orderGroup);
                    }
                }
                request.getSession().setAttribute("myorders", myOrders);
                request.getRequestDispatcher("/WEB-INF/jsp/user/myorders.jsp").forward(request, response);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try { if (null!=rst) rst.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=pstmt) pstmt.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=dbconn) dbconn.close();}
                catch (SQLException e) {e.printStackTrace();}
            }
        } else {
            response.sendRedirect("/bookshop/login");
        }
    }
}
