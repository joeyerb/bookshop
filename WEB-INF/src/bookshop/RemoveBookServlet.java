package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;

@WebServlet("/manage/removebook")
public class RemoveBookServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }

    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        String bookid = request.getParameter("bookid").trim();
        try {
            dbconn = dataSource.getConnection();
            String sql = "DELETE FROM books WHERE bookid = '" + bookid + "'";
            pstmt = dbconn.prepareStatement(sql);
            int changedLine = pstmt.executeUpdate();
            response.sendRedirect("/bookshop/");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { if (null!=pstmt) pstmt.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn) dbconn.close();}
            catch (SQLException e) {e.printStackTrace();}
        }
    }
}
