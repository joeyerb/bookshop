package bookshop;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.naming.*;

import bookshop.User;

@WebServlet(name = "Register", urlPatterns = {"/register"})
public class UserRegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String passwd = request.getParameter("password");
        String passwd2 = request.getParameter("confirm-password");
        if (!passwd.equals(passwd2)) {
            request.getSession().setAttribute("errormsg", "Password not match!" + passwd + ":" + passwd2 + ";");
            request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
        try {
            dbconn = dataSource.getConnection();
            String sql = "INSERT INTO users VALUES(?, ?, ?, ?, ?, ?)";
            pstmt = dbconn.prepareStatement(sql);
            pstmt.setString(1, email);
            pstmt.setString(2, passwd);
            pstmt.setString(3, email);
            pstmt.setString(4, username);
            pstmt.setString(5, address);
            pstmt.setString(6, phone);
            int changedLine = pstmt.executeUpdate();
            pstmt.close();
            dbconn.close();
            if (changedLine == 0) {
                request.getSession().setAttribute("errormsg", "Username already exists.");
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            } else {
                User user = new User(email, passwd, username, address, phone, true);
                request.getSession().setAttribute("user", user);
                request.getRequestDispatcher("/WEB-INF/jsp/welcome.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { if (null!=pstmt) pstmt.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn) dbconn.close();}
            catch (SQLException e) {e.printStackTrace();}
        }
    }
}

