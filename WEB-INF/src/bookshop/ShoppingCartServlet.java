package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;
import bookshop.Order;

@WebServlet("/cart")
public class ShoppingCartServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/user/shopping_cart.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {
        String action = request.getParameter("action");
        ArrayList<Order> orderList = new ArrayList<Order>();
        Object _orderList = request.getSession().getAttribute("orderList");
        if (action.equals("remove")) {
            String orderid = request.getParameter("orderid").trim();
            if (_orderList != null) {
                orderList = (ArrayList<Order>) _orderList;
                for (int i=0; i<orderList.size(); i++)
                    if (orderList.get(i).getOrderid().trim().equals(orderid)) {
                        orderList.remove(i);
                    }
                request.getSession().setAttribute("orderList", orderList);
                response.sendRedirect("/bookshop/cart");
            }
        } else if (action.equals("confirm")) {
            float totalprice = Float.parseFloat(request.getParameter("totalprice"));
            if (_orderList != null) {
                orderList = (ArrayList<Order>) _orderList;
                String sql = "INSERT INTO orders(orderid, bookid, qty, perprice, totalprice, mainid, userid, orderdate, email, address, phone, title, cover, status) VALUES";
                for (int i=0; i<orderList.size(); i++) {
                    Order order = orderList.get(i);
                    sql += "('" + order.getOrderid() + "', '"
                        + order.getBookid() + "', "
                        + order.getQty() + ", "
                        + order.getPerprice() + ", "
                        + totalprice + ", '"
                        + order.getMainid() + "', '"
                        + order.getUserid() + "', '"
                        + order.getOrderdate() + "', '"
                        + order.getEmail() + "', '"
                        + order.getAddress() + "', '"
                        + order.getPhone() + "', '"
                        + order.getTitle() + "', '"
                        + order.getCover() + "', false)";
                    if (i < orderList.size() - 1)
                        sql += ", ";
                }
                try {
                    dbconn = dataSource.getConnection();
                    pstmt = dbconn.prepareStatement(sql);
                    int changedLine = pstmt.executeUpdate();
                    if (changedLine > 0) {
                        request.getSession().removeAttribute("orderList");
                        response.sendRedirect("/bookshop/myorders");
                    } else {
                        request.getSession().setAttribute("errormsg", sql);
                        request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try { if (null!=pstmt) pstmt.close();}
                    catch (SQLException e) {e.printStackTrace();}
                    try { if (null!=dbconn) dbconn.close();}
                    catch (SQLException e) {e.printStackTrace();}
                }
            }
        } else {
            request.getSession().removeAttribute("orderList");
            response.sendRedirect("/bookshop/");
        }
    }
}

