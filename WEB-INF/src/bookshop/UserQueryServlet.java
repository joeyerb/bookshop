package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.User;

@WebServlet("/userquery.do")
public class UserQueryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {
        String userid = request.getParameter("userid");
        ResultSet rst = null;
        try {
            dbconn = dataSource.getConnection();
            String sql = "SELECT * FROM users WHERE userid = ?";
            pstmt = dbconn.prepareStatement(sql);
            pstmt.setString(1, userid);
            rst = pstmt.executeQuery();
            if (rst.next()) {
                User user = new User();
                user.setUserid(rst.getString("userid"));
                user.setUsername(rst.getString("username"));
                user.setPasswd(rst.getString("password"));
                user.setEmail(rst.getString("email"));
                user.setAddress(rst.getString("address"));
                user.setPhone(rst.getString("phone"));
                user.setStatus(rst.getBoolean("status"));
                request.getSession().setAttribute("user", user);
                request.getRequestDispatcher("/WEB-INF/jsp/profile.jsp").forward(request, response);
            } else {
                request.getSession().setAttribute("errormsg", "no such user" + userid);
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }	
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { if (null!=rst) rst.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=pstmt) pstmt.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn) dbconn.close();}
            catch (SQLException e) {e.printStackTrace();}
        }
    }
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
        throws ServletException, IOException {
        doGet(request, response);
    }
}
