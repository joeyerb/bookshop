package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;

@WebServlet("/product/details")
public class BookDetailsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {
        Book book = new Book();
        boolean found = false;
        String bookid = request.getParameter("id").trim();
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList = (ArrayList<Book>)request.getSession().getAttribute("bookList");
        for (Book tmp: bookList)
            if (bookid.equals(tmp.getBookid().trim())) {
                found = true;
                request.setAttribute("book", tmp);
                request.getRequestDispatcher("/WEB-INF/jsp/book_info.jsp").forward(request, response);
            }
        if (!found) {
            ResultSet rst = null;
            try {
                dbconn = dataSource.getConnection();
                String sql = "SELECT * FROM books WHERE bookid = ?";
                pstmt = dbconn.prepareStatement(sql);
                pstmt.setString(1, bookid.trim());
                rst = pstmt.executeQuery();
                if (rst.next()) {
                    book.setBookid(rst.getString("bookid"));
                    book.setTitle(rst.getString("title"));
                    book.setAuthor(rst.getString("author"));
                    book.setPublisher(rst.getString("publisher"));
                    book.setPublishdate(rst.getString("publishdate"));
                    book.setIsbn(rst.getString("isbn"));
                    book.setCategory(rst.getString("category"));
                    book.setDescription(rst.getString("description").replaceAll("(\r\n|\n)", "<br />"));
                    book.setAuthorinfo(rst.getString("authorinfo").replaceAll("(\r\n|\n)", "<br />"));
                    book.setContent(rst.getString("content").replaceAll("(\r\n|\n)", "<br />"));
                    book.setCover(rst.getString("cover"));
                    book.setImages(rst.getString("images"));
                    book.setPrice(rst.getFloat("price"));
                    book.setRating(rst.getFloat("rating"));
                    book.setInstock(rst.getInt("instock"));
                    bookList.add(book);
                    request.setAttribute("book", book);
                    request.getRequestDispatcher("/WEB-INF/jsp/book_info.jsp").forward(request, response);
                } else {
                    request.getSession().setAttribute("errormsg", "No such book in library! Bookid:" + bookid + ".");
                    request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try { if (null!=rst) rst.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=pstmt) pstmt.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=dbconn) dbconn.close();}
                catch (SQLException e) {e.printStackTrace();}
            }
        }
    }
}
