package bookshop;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.naming.*;

import bookshop.User;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class UserLoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn0;
    private PreparedStatement pstmt0;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        Object _user = request.getSession().getAttribute("user");
        if (_user != null)
            response.sendRedirect("/bookshop/");
        else
            request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String userid = request.getParameter("username");
        String passwd = request.getParameter("password");
        ResultSet rst = null;
        try {
            dbconn = dataSource.getConnection();
            String sql = "SELECT * FROM users WHERE userid = ?";
            pstmt = dbconn.prepareStatement(sql);
            pstmt.setString(1, userid);
            rst = pstmt.executeQuery();
            if (rst.next()) {
                if (passwd.equals(rst.getString("password").trim())) {
                    dbconn0 = dataSource.getConnection();
                    String sql0 = "UPDATE users SET status = TRUE WHERE userid = ?";
                    pstmt0 = dbconn0.prepareStatement(sql0);
                    pstmt0.setString(1, userid);
                    int changedLine = pstmt0.executeUpdate();
                    if (changedLine == 0) {
                        request.getSession().setAttribute("errormsg", "test error");
                        request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                    } else {
                        User user = new User(userid, passwd);
                        user.setUsername(rst.getString("username"));
                        user.setAddress(rst.getString("address"));
                        user.setPhone(rst.getString("phone"));
                        user.setRole(rst.getString("role"));
                        user.setStatus(rst.getBoolean("status"));
                        request.getSession().setAttribute("user", user);
                        response.sendRedirect("/bookshop/");
                    }
                } else {
                    request.getSession().setAttribute("errormsg", "login failed 2");
                    request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                }
            } else {
                request.getSession().setAttribute("errormsg", "login failed 1");
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try { if (null!=pstmt0) pstmt0.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn0) dbconn0.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=rst) rst.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=pstmt) pstmt.close();}
            catch (SQLException e) {e.printStackTrace();}
            try { if (null!=dbconn) dbconn.close();}
            catch (SQLException e) {e.printStackTrace();}
        }
    }
}
