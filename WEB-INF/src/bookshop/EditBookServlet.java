package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;

@WebServlet("/manage/editbook")
public class EditBookServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }

    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        boolean found = false;
        String bookid = request.getParameter("id").trim();
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList = (ArrayList<Book>)request.getSession().getAttribute("bookList");
        for (Book tmp: bookList)
            if (bookid.equals(tmp.getBookid().trim())) {
                found = true;
                request.setAttribute("book", tmp);
                request.getRequestDispatcher("/WEB-INF/jsp/manage/add_book.jsp").forward(request, response);
            }
        if (!found) {
            request.getSession().setAttribute("errormsg", "Book not found: " + bookid);
            request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        boolean found = false;
        Book book = new Book();
        String bookid = request.getParameter("id").trim();
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String publisher = request.getParameter("publisher");
        String publishdate = request.getParameter("publishdate");
        String isbn = request.getParameter("isbn");
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        String authorinfo = request.getParameter("authorinfo");
        String content = request.getParameter("content");
        String cover = request.getParameter("cover");
        String price = request.getParameter("price");

        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList = (ArrayList<Book>)request.getSession().getAttribute("bookList");
        for (Book tmp: bookList)
            if (bookid.equals(tmp.getBookid().trim())) {
                found = true;
                book = tmp;
                tmp.setPrice( Float.parseFloat(price) );
            }
        if (!found) {
            request.getSession().setAttribute("errormsg", bookid);
            request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        } else {
            book.setImages("");
            book.setPrice(Float.parseFloat(price));
            String sql = "UPDATE books SET price = " + price;
            if (!title.equals(book.getTitle())) {
                book.setTitle(title);
                sql += ", title = '" + title + "'";
            }
            if (!author.equals(book.getAuthor())) {
                book.setAuthor(author);
                sql += ", author = '" + author + "'";
            }
            if (!publisher.equals(book.getPublisher())) {
                book.setPublisher(publisher);
                sql += ", publisher = '" + publisher + "'";
            }
            if (!publishdate.equals(book.getPublishdate())) {
                sql += ", publishdate = '" + publishdate + "'";
                book.setPublishdate(publishdate);
            }
            if (!category.equals(book.getCategory())) {
                book.setCategory(category);
                sql += ", category = '" + category + "'";
            }
            if (!isbn.equals(book.getIsbn())) {
                book.setIsbn(isbn);
                sql += ", isbn = '" + isbn + "'";
            }
            if (!cover.equals(book.getCover())) {
                book.setCover(cover);
                sql += ", cover = '" + cover + "'";
            }
            if (!content.equals(book.getContent())) {
                book.setContent(content);
                sql += ", content = '" + content + "'";
            }
            if (!description.equals(book.getDescription())) {
                book.setDescription(description);
                sql += ", description = '" + description + "'";
            }
            if (!authorinfo.equals(book.getAuthorinfo())) {
                book.setAuthorinfo(authorinfo);
                sql += ", authorinfo = '" + authorinfo + "'";
            }
            sql += " WHERE bookid = '" + bookid + "'";
            try {
                dbconn = dataSource.getConnection();
                pstmt = dbconn.prepareStatement(sql);
                int changedLine = pstmt.executeUpdate();
                response.sendRedirect("/bookshop/product/details?id=" + bookid);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try { if (null!=pstmt) pstmt.close();}
                catch (SQLException e) {e.printStackTrace();}
                try { if (null!=dbconn) dbconn.close();}
                catch (SQLException e) {e.printStackTrace();}
            }
        }
    }
}


