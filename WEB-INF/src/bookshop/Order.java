package bookshop;
import java.io.Serializable;

public class Order implements Serializable
{
    private String orderid;
    private String bookid;
    private int qty;

    private String title;
    private String cover;
    private float perprice;

    private String mainid;
    private String userid;
    private String orderdate;

    private boolean status;

    // Optional
    private String email;
    private String address;
    private String phone;

    public Order() {
        this.qty = 1;
    }

    public String getOrderid() {
        return this.orderid;
    }

    public String getBookid() {
        return this.bookid;
    }

    public String getUserid() {
        return this.userid;
    }

    public String getOrderdate() {
        return this.orderdate;
    }

    public int getQty() {
        return this.qty;
    }

    public float getPerprice() {
        return this.perprice;
    }

    public String getEmail() {
        return this.email;
    }

    public String getAddress() {
        return this.address;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getMainid() {
        return this.mainid;
    }

    public String getTitle() {
        return this.title;
    }

    public String getCover() {
        return this.cover;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public void setBookid(String bookid) {
        this.bookid = bookid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setPerprice(float perprice) {
        this.perprice = perprice;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMainid(String mainid) {
        this.mainid = mainid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
