package bookshop;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import java.util.zip.CRC32;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;
import bookshop.Order;

@WebServlet("/addtocart.do")
public class AddtoCartServlet extends HttpServlet {
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        boolean found = false;
        String bookid = request.getParameter("bookid").trim();
        String cover = request.getParameter("cover");
        String title = new String(request.getParameter("title").getBytes("ISO-8859-1"), "UTF-8");
        float price = Float.parseFloat(request.getParameter("price"));

        User user = new User();
        Object _user = request.getSession().getAttribute("user");
        if (_user != null) {
            user = (User) _user;

            ArrayList<Order> orderList = new ArrayList<Order>();
            Object _orderList = request.getSession().getAttribute("orderList");
            if (_orderList != null) {
                orderList = (ArrayList<Order>) _orderList;
                for (Order tmp: orderList)
                    if (bookid.equals(tmp.getBookid().trim())) {
                        found = true;
                        tmp.setQty(tmp.getQty()+1);
                    }
            }
            if (!found) {
                Order order = new Order();

                Long ordertime = System.currentTimeMillis();
                CRC32 crc = new CRC32();
                crc.update(user.getUserid().trim().getBytes());
                String orderid = Long.toString(ordertime + crc.getValue()*100000);

                order.setOrderdate(Long.toString(ordertime));
                order.setOrderid(orderid);

                order.setBookid(bookid);
                order.setTitle(title);
                order.setCover(cover);
                order.setPerprice(price);

                if (orderList.isEmpty())
                    order.setMainid(orderid);
                else
                    order.setMainid(orderList.get(0).getMainid());

                order.setUserid(user.getUserid());
                order.setEmail(user.getEmail());
                order.setAddress(user.getAddress());
                order.setPhone(user.getPhone());

                orderList.add(order);
            }
            request.getSession().setAttribute("orderList", orderList);
            response.sendRedirect("/bookshop/product/details?id="+bookid);
        } else {
            response.sendRedirect("/bookshop/login");
        }
    }
}
