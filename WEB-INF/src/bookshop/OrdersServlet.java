package bookshop;

import java.io.*;
import java.sql.*;

import javax.sql.DataSource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import javax.naming.*;
import bookshop.Book;
import bookshop.User;
import bookshop.Order;
import bookshop.OrderGroup;

@WebServlet("/manage/orders")
public class OrdersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    DataSource dataSource;
    private Connection dbconn;
    private PreparedStatement pstmt;
    public void init() {
        try {
            Context context = new InitialContext();
            dataSource = (DataSource)context.lookup("java:comp/env/jdbc/bookshopDS");
        } catch(NamingException ne) {
            log("Exception:"+ne);
        }
    }
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        User user = new User();
        Object _user = request.getSession().getAttribute("user");
        if (_user != null) {
            user = (User) _user;
            ResultSet rst = null;
            if ("admin".equals(user.getRole())) {
                try {
                    dbconn = dataSource.getConnection();
                    String sql = "SELECT * FROM orders ORDER BY orderdate DESC";
                    pstmt = dbconn.prepareStatement(sql);
                    rst = pstmt.executeQuery();
                    HashMap<String, OrderGroup> recentOrders = new HashMap<String, OrderGroup>();
                    while (rst.next()) {
                        Order order = new Order();
                        order.setOrderid(rst.getString("orderid"));
                        order.setBookid(rst.getString("bookid"));
                        order.setOrderdate(rst.getString("orderdate"));

                        order.setTitle(rst.getString("title"));
                        order.setCover(rst.getString("cover"));
                        order.setPerprice(rst.getFloat("perprice"));
                        order.setQty(rst.getInt("qty"));

                        String mainid = rst.getString("mainid");
                        order.setMainid(mainid);

                        order.setUserid(rst.getString("userid"));
                        order.setEmail(rst.getString("email"));
                        order.setAddress(rst.getString("address"));
                        order.setPhone(rst.getString("phone"));

                        order.setStatus(rst.getBoolean("status"));

                        OrderGroup orderGroup = recentOrders.get(mainid);
                        if (orderGroup != null) {
                            orderGroup.add(order);
                        } else {
                            orderGroup = new OrderGroup(order);
                            recentOrders.put(mainid, orderGroup);
                        }
                    }
                    request.getSession().setAttribute("recentorders", recentOrders);
                    request.getRequestDispatcher("/WEB-INF/jsp/manage/orders.jsp").forward(request, response);
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try { if (null!=rst) rst.close();}
                    catch (SQLException e) {e.printStackTrace();}
                    try { if (null!=pstmt) pstmt.close();}
                    catch (SQLException e) {e.printStackTrace();}
                    try { if (null!=dbconn) dbconn.close();}
                    catch (SQLException e) {e.printStackTrace();}
                }
            } else {
                request.getSession().setAttribute("errormsg", "Permission denied.");
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }
        } else {
            response.sendRedirect("/bookshop/login");
        }
    }
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {

        User user = new User();
        Object _user = request.getSession().getAttribute("user");
        if (_user != null) {
            user = (User) _user;
            if ("admin".equals(user.getRole())) {
                try {
                    String action = request.getParameter("action");
                    String mainid = request.getParameter("mainid");
                    String sql = "";
                    if (action.equals("confirm"))
                        sql = "UPDATE orders SET status = true WHERE mainid = '" + mainid + "'";
                    else if (action.equals("denial"))
                        sql = "DELETE FROM orders WHERE mainid = '" + mainid + "'";
                    dbconn = dataSource.getConnection();
                    pstmt = dbconn.prepareStatement(sql);
                    int changedLine = pstmt.executeUpdate();
                    if (changedLine > 0) {
                        response.sendRedirect("/bookshop/manage/orders");
                    } else {
                        request.getSession().setAttribute("errormsg", "Nothing changed.");
                        request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    try { if (null!=pstmt) pstmt.close();}
                    catch (SQLException e) {e.printStackTrace();}
                    try { if (null!=dbconn) dbconn.close();}
                    catch (SQLException e) {e.printStackTrace();}
                }
            } else {
                request.getSession().setAttribute("errormsg", "Permission denied.");
                request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }
        } else {
            response.sendRedirect("/bookshop/login");
        }
    }
}
