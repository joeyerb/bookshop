package bookshop;
import java.io.Serializable;

public class User implements Serializable
{
    private String userid;
    private String passwd;
    private String username;
    private String email;
    private String address;
    private String phone;
    private String role;
    private boolean status;

    public User() {}
    public User(String userid, String passwd) {
        this.userid = userid;
        this.passwd = passwd;
        this.email = userid;
        this.status = false;
    }
    public User(String userid, String passwd, String username, boolean status) {
        this.userid = userid;
        this.passwd = passwd;
        this.email = userid;
        this.username = username;
        this.status = status;
    }
    public User(String userid, String passwd, String username, String address, String phone, boolean status) {
        this.userid = userid;
        this.passwd = passwd;
        this.email = userid;
        this.username = username;
        this.address = address;
        this.phone = phone;
        this.status = status;
    }

    public String getUserid() {
        return this.userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPasswd() {
        return this.passwd;
    }
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public boolean getStatus() {
        return this.status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return this.phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getRole() {
        return this.role;
    }
    public void setRole(String role) {
        this.role = role;
    }
}
