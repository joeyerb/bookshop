<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,bookshop.User,bookshop.Book,bookshop.Order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>${book.title}</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>

  <body>

    <% Book book = (Book) request.getAttribute("book"); %>

    <%@ include file="_header.jsp" %>

    <div class="ui raised text container segment">
      <br>
      <div class="ui items">
        <div class="item">
          <div class="image">
            <img src="/bookshop/images/${book.cover}">
          </div>
          <div class="content">
            <span class="header">${book.title}</span>
            <div class="meta">
              <a class="ui orange right ribbon label large">￥${book.price}</a>
            </div>
            <div class="description">
              <div class="ui list">
                <div class="item">
                  <i class="user icon" data-content="作者"></i>
                  <div class="content">
                    ${book.author}
                  </div>
                </div>
                <div class="item">
                  <i class="book icon" data-content="出版社"></i>
                  <div class="content">
                    ${book.publisher}
                  </div>
                </div>
                <div class="item">
                  <i class="calendar icon" data-content="出版日期"></i>
                  <div class="content">
                    ${book.publishdate}
                  </div>
                </div>
                <div class="item">
                  <i class="tag icon" data-content="類別"></i>
                  <div class="content">
                    ${book.category}
                  </div>
                </div>
                <div class="item">
                  <i class="barcode icon" data-content="ISBN"></i>
                  <div class="content">
                    ${book.isbn}
                  </div>
                </div>
                <div class="item">
                  <i class="thumbs up icon" data-content="評分"></i>
                  <div class="content">
                    <%
                    out.println("<div class=\"ui star rating\" data-rating=\"" + (int)book.getRating() +"\" data-max-rating=\"5\"></div>");
                    %> <span>${book.rating}</span>
                  </div>
                </div>
                <div class="item">
                  <div id="addtocart_btn" class="ui animated fade blue button" tabindex="0">
                    <div class="hidden content">￥${book.price}</div>
                    <div class="visible content">
                      <i class="shop icon"></i>
                      Add to Cart
                    </div>
                  </div>
                  <%
                      if ( _user != null) {
                          User user = (User) _user;
                          if ("admin".equals(user.getRole())) {
                              out.println("<a class=\"ui orange button\" href=\"/bookshop/manage/editbook?id="+book.getBookid()+"\"> <i class=\"edit icon\"></i> Edit </a>");
                              out.println("<button id=\"remove_btn\" class=\"ui red button\"> <i class=\"remove icon\"></i> Remove </button>");
                          }
                      }
                  %>
                </div>
              </div>
            </div>
            <div class="extra">
            </div>
          </div>
        </div>
      </div>

      <div class="ui styled fluid accordion">
        <div class="title">
          <i class="dropdown icon"></i>
          Content
        </div>
        <div class="content">
          <p>${book.content}</p>
        </div>
        <div class="acitve title">
          <i class="dropdown icon"></i>
          Description
        </div>
        <div class="active content">
          <p>${book.description}</p>
        </div>
        <div class="title">
          <i class="dropdown icon"></i>
          About Author
        </div>
        <div class="content">
          <p>${book.authorinfo}</p>
        </div>
        <div class="title">
          <i class="dropdown icon"></i>
          Images
        </div>
        <div class="content">
          <%
          Object _imgs = book.getImages();
          if (_imgs != null) {
              String imagestr = (String)_imgs;
              String[] images = new String[0];
              images = imagestr.split(",");
              for (String img: images) {
                  if (!img.equals(""))
                      out.println( "<img class=\"ui fluid image\" src=\"" + img + "\">" );
              }
          }
          %>
        </div>
        <div class="title">
          <i class="dropdown icon"></i>
          Comments
        </div>
        <div class="content">
        </div>
      </div>

      <br>

    </div>

    <form id="addtocart_form" action="/bookshop/addtocart.do" method="post">
      <input type="hidden" name="bookid" value="${book.bookid}">
      <input type="hidden" name="title" value="${book.title}">
      <input type="hidden" name="cover" value="${book.cover}">
      <input type="hidden" name="price" value="${book.price}">
    </form>
    <form id="remove_form" action="/bookshop/manage/removebook" method="post">
      <input type="hidden" name="bookid" value="${book.bookid}">
    </form>

    <script>
      $('.ui.accordion').accordion();
      $('.ui.rating').rating('disable');
      $('.item .icon').popup();

      $('#addtocart_btn')
          .click(function(event){
              event.preventDefault();
              $('#addtocart_form').submit();
          });
      $('#remove_btn')
          .click(function(event){
              event.preventDefault();
              $('#remove_form').submit();
          });
    </script>

  </body>
</html>
