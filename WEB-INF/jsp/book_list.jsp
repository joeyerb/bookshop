<%@ page pageEncoding="UTF-8"%>
      <div class="ui items">
        <c:forEach var="book" items="${sessionScope.bookList}"
                              varStatus="status">
        <div class="item">
          <div class="image">
            <img src="/bookshop/images/${book.cover}">
          </div>
          <div class="content">
            <a class="header" href="/bookshop/product/details?id=${book.bookid}">${book.title}</a>
            <div class="meta">
              <a class="ui orange right ribbon label large">￥${book.price}</a>
            </div>
            <div class="description">
              <div class="ui list">
                <div class="item">
                  <i class="user icon" data-content="作者"></i>
                  <div class="content">
                    ${book.author}
                  </div>
                </div>
                <div class="item">
                  <i class="book icon" data-content="出版社"></i>
                  <div class="content">
                    ${book.publisher}
                  </div>
                </div>
                <div class="item">
                  <i class="calendar icon" data-content="出版日期"></i>
                  <div class="content">
                    ${book.publishdate}
                  </div>
                </div>
                <div class="item">
                  <i class="tag icon" data-content="類別"></i>
                  <div class="content">
                    ${book.category}
                  </div>
                </div>
              </div>
            </div>
            <div class="extra">
            </div>
          </div>
        </div>
        <div class="ui divider"></div>
        </c:forEach>
      </div>
