<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,bookshop.User,bookshop.Book,bookshop.Order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Search Result</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>
  <body>
    <%@ include file="_header.jsp" %>
    <div class="ui raised text container segment">
      <h3>Search Result -- "${keywords}"</h3>
      <%@ include file="book_list.jsp" %>
    </div>
  </body>
</html>

