<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,bookshop.User,bookshop.Book,bookshop.Order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>${book.title}</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>

  <body>

    <%@ include file="../_header.jsp" %>

    <% Book book = (Book) request.getAttribute("book"); %>
    <div class="ui raised text container segment">
      <h3><%= book.getBookid()!=null?"Edit Book":"New Book" %></h3>
      <%
      if(book.getBookid()!=null) {
      out.println("<a class=\"ui right corner label\" href=\"/bookshop/product/details?id=" + book.getBookid() + "\"> <i class=\"undo icon\"></i> </a>");
      }
      %>
      <form class="ui form" method="post">
        <div class="ui field left icon input">
          <input name="title" placeholder="Title" type="text" value="${book.title}">
          <i class="book icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="author" placeholder="Author" type="text" value="${book.author}">
          <i class="user icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="publisher" placeholder="Publisher" type="text" value="${book.publisher}">
          <i class="print icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="publishdate" placeholder="Publish Date" type="text" value="${book.publishdate}">
          <i class="calendar icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="category" placeholder="Category" type="text" value="${book.category}">
          <i class="tag icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="isbn" placeholder="ISBN" type="text" value="${book.isbn}">
          <i class="barcode icon"></i>
        </div>
        <div class="ui field left icon input">
          <input name="price" placeholder="Price" type="text" value="${book.price}">
          <i class="money icon"></i>
        </div>
        <div class="ui icon labeled input">
          <i class="file image outline icon"></i>
          <div class="ui label">
            http://
          </div>
          <input name="cover" placeholder="Cover URL" type="text" value="${book.cover}">
        </div>
        <br>
        <br>
        <div class="field">
          <label>Content</label>
          <textarea name="content">${book.content}</textarea>
        </div>
        <div class="field">
          <label>Description</label>
          <textarea name="description">${book.description}</textarea>
        </div>
        <div class="field">
          <label>About Author</label>
          <textarea name="authorinfo">${book.authorinfo}</textarea>
        </div>
        <button class="ui blue button" type="submit">Submit</button>
        <button class="ui button" type="reset">Cancel</button>
      </form>
    </div>
  </body>
</html>


