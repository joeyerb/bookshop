    <div class="ui attached stackable menu">
      <div class="ui container">
        <a class="item" href="/bookshop/">
          <i class="home icon"></i> Home
        </a>
        <a class="item" href="/bookshop/">
          <i class="grid layout icon"></i> Browse
        </a>
        <a class="item" href="/bookshop/cart">
          <i class="shop icon"></i> Shopping Cart
          <%
              Object _orderList = session.getAttribute("orderList");
              if ( _orderList != null ) {
                  ArrayList<Order> orderList = (ArrayList<Order>) _orderList;
                  out.println("<div class=\"ui red label\">" + orderList.size() + "</div>");
              }
          %>
        </a>
        <%
            Object _user = session.getAttribute("user");
            if ( _user != null) {
                User user = (User) _user;
                out.println("<div class=\"ui simple dropdown item\">");
                out.println("  <i class=\"user icon\"></i> " + user.getUsername());
                out.println("  <i class=\"dropdown icon\"></i>");
                out.println("  <div class=\"menu\">");
                out.println("    <a class=\"item\" href=\"/bookshop/myorders\"><i class=\"list icon\"></i> Orders</a>");
                out.println("    <a class=\"item\"><i class=\"edit icon\"></i> Profile</a>");
                out.println("    <a class=\"item\" href=\"/bookshop/logout\"><i class=\"sign out icon\"></i> Logout</a>");
                out.println("  </div>");
                out.println("</div>");

                if ("admin".equals(user.getRole())) {
                    out.println("<div class=\"ui simple dropdown item\">");
                    out.println("  <i class=\"list icon\"></i> Manage");
                    out.println("  <i class=\"dropdown icon\"></i>");
                    out.println("  <div class=\"menu\">");
                    out.println("    <a class=\"item\" href=\"/bookshop/manage/addbook\"><i class=\"book icon\"></i> Add Book</a>");
                    out.println("    <a class=\"item\" href=\"/bookshop/manage/orders\"><i class=\"calculator icon\"></i> Orders</a>");
                    out.println("  </div>");
                    out.println("</div>");
                }
            } else {
                out.println(" <a class=\"item\" href=\"/bookshop/login\"><i class=\"sign in icon\"></i> Login</a> ");
            }
        %>
        <div class="right item">
          <form class="ui action input" id="search_form" action="/bookshop/bookquery.do" method="post">
            <input placeholder="Search..." type="text" name="keywords">
            <div id="search_btn" class="ui button">Search</div>
          </form>
        </div>

      </div>
    </div>
    <script>
      $('#search_btn')
          .click(function(event){
              event.preventDefault();
              $('#search_form').submit();
          });
    </script>
