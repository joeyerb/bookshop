<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,java.text.*,bookshop.User,bookshop.Book,bookshop.Order,bookshop.OrderGroup" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>My Orders</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>
  <body>
    <%@ include file="../_header.jsp" %>
    <div class="ui raised text container segment">
      <h3>My Orders</h3>
      <div class="ui">
        <c:forEach var="entry" items="${sessionScope.myorders}"
                               varStatus="status">
        <div class="ui middle aligned divided list segment">
          <c:set var="mainorder" value="${entry.getValue().getMainOrder()}" />
          <div class="item">
            <br>
            <h3 class="ui header">
              <div class="content">ID : ${entry.getKey()}</div>
              <div class="sub header">
                <i class="calendar icon"></i>
                <%
                    Order mainorder = (Order)pageContext.getAttribute("mainorder");
                    Date orderdate = new Date(Long.parseLong(mainorder.getOrderdate()));
                    DateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm:ss zzz");
                    out.println(df.format(orderdate));
                %>
              </div>
            </h3>
            <div class="ui top right attached large label"><%= mainorder.getStatus()?"Finished":"Not Finished" %></div>
          </div>
          <c:set var="totalprice" value="${0}"/>
          <c:forEach var="order" items="${entry.getValue().getOrders()}">
          <c:set var="totalprice" value="${totalprice + order.perprice * order.qty}" />
          <div class="item">
            <br>
            <img class="ui image" src="/bookshop/images/${order.cover}" width="80" height="80">
            <div class="content">
              <a href="/bookshop/product/details?id=${order.bookid}">${order.title}</a>
            </div>
            <div class="right floated content" style="padding-top: 27px;">
              <p class="ui tag teal label">￥${order.perprice}<span class="detail">x ${order.qty}</span></p>
            </div>
          </div>
          </c:forEach>
          <div class="item">
            <div class="ui list">
              <c:set var="mainorder" value="${entry.getValue().getMainOrder()}" />
              <div class="item">
                <i class="user icon"></i>
                <div class="content">${mainorder.getUserid()}</div>
              </div>
              <div class="item">
                <i class="marker icon"></i>
                <div class="content">${mainorder.getAddress()}</div>
              </div>
              <div class="item">
                <i class="phone icon"></i>
                <div class="content">${mainorder.getPhone()}</div>
              </div>
              <fmt:setLocale value="zh_CN"/>
            </div>
            <div class="ui bottom right ribbon large green label" style="top: -50px;">Total: <fmt:formatNumber type="currency" value="${entry.getValue().getTotalprice()}" /></div>
          </div>
        </div>
        <br>
        </c:forEach>
      </div>
    </div>
  </body>
</html>
