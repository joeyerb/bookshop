<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,bookshop.User,bookshop.Book,bookshop.Order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Book Shop</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>
  <body>
    <%@ include file="../_header.jsp" %>
    <div class="ui raised text container segment">
      <h3>Shopping Cart</h3>
      <div class="ui middle aligned divided list">
        <c:set var="totalprice" value="${0}"/>
        <c:forEach var="order" items="${sessionScope.orderList}"
                               varStatus="status">
        <c:set var="totalprice" value="${totalprice + order.perprice * order.qty}" />
        <div class="item">
          <br>
          <img class="ui image" src="/bookshop/images/${order.cover}" width="80" height="80">
          <div class="content">
            <p>${order.title}</p>
            <p class="ui tag teal label">￥${order.perprice}<span class="detail">x ${order.qty}</span></p>
          </div>
          <div class="right floated content" style="padding-top: 20px;">
            <div id="remove_btn" class="ui button">Remove</div>
            <form class="ui action input" id="remove_form" action="/bookshop/cart" method="post">
              <input type="hidden" name="action" value="remove">
              <input type="hidden" name="orderid" value="${order.orderid}">
            </form>
          </div>
        </div>
        </c:forEach>
        <div class="item">
          <br>
        </div>
      </div>
      <fmt:setLocale value="zh_CN"/>
      <div class="ui bottom left attached large green label">Total: <fmt:formatNumber type="currency" value="${totalprice}" /></div>
      <a class="ui bottom right attached large orange label" id="confirm_btn"><i class="ui shop icon"></i>Buy Now</a>
      <form class="ui action input" id="confirm_form" action="/bookshop/cart" method="post">
        <input type="hidden" name="totalprice" id="totalprice" value="${totalprice}">
        <input type="hidden" name="action" value="confirm">
      </form>
    </div>
    <script>
      $('#remove_btn')
          .click(function(event){
              event.preventDefault();
              $('#remove_form').submit();
          });
      $('#confirm_btn')
          .click(function(event){
              event.preventDefault();
              if ($('#totalprice').val() > 0)
                  $('#confirm_form').submit();
              else
                  alert('Nothing Left');
          });
    </script>
  </body>
</html>
