<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*,bookshop.User,bookshop.Book,bookshop.Order" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Error!</title>
    <link rel="stylesheet" type="text/css" href="/bookshop/semantic/dist/semantic.min.css">
    <script src="/bookshop/static/js/jquery.min.js"></script>
    <script src="/bookshop/semantic/dist/semantic.min.js"></script>
  </head>
  <body>
    <%@ include file="_header.jsp" %>
    <div class="ui raised text container segment">
      <p></p>
      <h3>${sessionScope.errormsg}</h3>
      <p></p>
    </div>
  </body>
</html>
